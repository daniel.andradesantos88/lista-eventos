# Lista Eventos
Cria lista de convidados para eventos e envia email ao convidado.

## Motivação
Projeto com viés de estudo, aprendizagem e prática em JAVA Spring Boot, bootstrap 4, JPA e Hibernate.

## Tecnologias utilizadas
- [JAVA] - Linguagem de programação 
- [SpringBoot]- Framework utilizado
- [Hibernate] - 
- [Bootstrap] - 
- [Eclipse] - Liguagens de marcação
- [Postgres]- Utilizado para fazer a busca dos dados no GitHub

## Autor
- **Daniel Andrade dos Santos** - [DanielASantos](https://github.com/DanielASantos)
